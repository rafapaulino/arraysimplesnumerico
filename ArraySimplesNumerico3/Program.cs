﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArraySimplesNumerico3
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] numero = new double[3];
            numero[0] = 1;
            numero[1] = 2;
            numero[2] = 3;

            double soma = numero.Sum();
            double media = numero.Average();

            Console.WriteLine("A soma dos elementos do Array é: " + soma);
            Console.WriteLine("A média dos elementos do Array é: " + media);
            Console.ReadKey();

            double[] numero2 = { 2, 4, 56, 78, 9, 20 };
            double soma2 = numero2.Sum();
            double media2 = numero2.Average();

            Console.WriteLine("A soma dos elementos do Array2 é: " + soma2);
            Console.WriteLine("A média dos elementos do Array2 é: " + media2);
            Console.ReadKey();
        }
    }
}
